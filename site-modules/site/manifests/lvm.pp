#set lvm based on external facts
class profiles::lvm {
include stdlib
include lvm
  if has_key($facts, 'lvm_system_vg_devices') and $facts['lvm_system_vg_devices'].is_a(Array) {
    physical_volume { $facts['lvm_system_vg_devices']: 
      ensure => present,
    }
    if has_key($facts, 'lvm_system_vg_name') {
      volume_group { $facts['lvm_system_vg_name']:
        ensure           => present,
        physical_volumes => $facts['lvm_system_vg_devices'],
      }
      if has_key($facts, 'lvm_lv_roots') and $facts['lvm_lv_roots'].is_a(Hash) {
        logical_volume { $facts['lvm_lv_root']['name']:
          ensure       => present,
          volume_group => $facts['lvm_system_vg_name'],
          size         => $facts['lvm_lv_root']['size'],
        }
      }
      if has_key($facts, 'lvm_lv_usr') and $facts['lvm_lv_usr'].is_a(Hash) {
        logical_volume { $facts['lvm_lv_usr']['name']:
          ensure       => present,
          volume_group => $facts['lvm_system_vg_name'],
          size         => $lvm_lv_usr['size'],
        }
      }
      if has_key($facts, 'lvm_lv_var') and $facts['lvm_lv_var'].is_a(Hash) {
        logical_volume { $facts['lvm_lv_var']['name']:
          ensure       => present,
          volume_group => $facts['lvm_system_vg_name'],
          size         => $facts['lvm_lv_var']['size'],
        }
      }
      if has_key($facts, 'lvm_lv_home') and $facts['lvm_lv_home'].is_a(Hash) {
#        #to define File['/home']
#        include profiles::default_useradd
        lvm::logical_volume { $facts['lvm_lv_home']['name']:
          ensure            => present,
          volume_group      => $facts['lvm_system_vg_name'],
          size              => $facts['lvm_lv_home']['size'],
#          mountpath         => '/home',
#          mountpath_require => true,
#          options           => $::lvm_lv_home['options'],
        }
        if has_key($facts['lvm_lv_home'],'options') {
          if $facts['lvm_lv_home']['options'] =~ /usrquota/ {
            quota::usr { '/home': timeout => 900 }
          }
          if $facts['lvm_lv_home']['options'] =~ /grpquota/ {
            quota::grp { '/home': timeout => 900 }
          }
        }
      }
      if has_key($facts, 'lvm_lv_swap') and $facts['lvm_lv_swap'].is_a(Hash) {
        logical_volume { $facts['lvm_lv_swap']['name']:
          ensure       => present,
          volume_group => $facts['lvm_system_vg_name'],
          size         => $lvm_lv_swap['size'],
        }
      }
      #optional file systems
      if has_key($facts, 'lvm_lv_opt') and $facts['lvm_lv_opt'].is_a(Hash) {
        file { '/opt':
          ensure => directory,
        }
        lvm::logical_volume { $facts['lvm_lv_opt']['name']:
          volume_group      => $facts['lvm_system_vg_name'],
          size              => $facts['lvm_lv_opt']['size'],
          fs_type           => $facts['lvm_lv_opt']['fstype'],
          mountpath         => '/opt',
          mountpath_require => true,
        }
      }
    }
  }
  #Optional data volume group
  if has_key($facts, 'lvm_data_vg_devices') and $facts['lvm_data_vg_devices'].is_a(Array) {
    physical_volume { $facts['lvm_data_vg_devices']: 
      ensure => present,
    }
    if has_key($facts, 'lvm_data_vg_name' ) {
      volume_group { $facts['lvm_data_vg_name']:
        ensure           => present,
        physical_volumes => $facts['lvm_data_vg_devices'],
      }
    }
  }
  #optional data file systems
  if has_key($facts, 'lvm_lv_data') and $facts['lvm_lv_data'].is_a(Hash) {
    lvm::logical_volume { $facts['lvm_lv_data']['name']:
      volume_group      => $facts['lvm_lv_data']['volume_group'],
      size              => $facts['lvm_lv_data']['size'],
      fs_type           => $facts['lvm_lv_data']['fstype'],
      mountpath         => $facts['lvm_lv_data']['path'],
      mountpath_require => true,
      options           => $facts['lvm_lv_data']['options'],
    }
    if has_key($facts['lvm_lv_data'],'options') {
      if $facts['lvm_lv_data']['options'] =~ /usrquota/ {
        quota::usr { $facts['lvm_lv_data']['path']: timeout => 900 }
      }
      if $facts['lvm_lv_data']['options'] =~ /grpquota/ {
        quota::grp { $facts['lvm_lv_data']['path']: timeout => 900 }
      }
    }
    file { $facts['lvm_lv_data']['path']:
      ensure => directory,
      owner  => $facts['lvm_lv_data']['owner'],
      group  => $facts['lvm_lv_data']['group'],
      mode   => $facts['lvm_lv_data']['mode'],
    }
  }
}
