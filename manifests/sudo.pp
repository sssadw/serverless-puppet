#Setup sudo

  include sudo
#  include sudo::configs

  sudo::conf { $sshgrp: content => "%${sshgrp} ALL=(ALL) NOPASSWD: ALL", }
  if $facts['os']['family'] == 'RedHat' {
    sudo::conf { 'wheel': content => "%wheel ALL=(ALL) NOPASSWD: ALL", }
  }


