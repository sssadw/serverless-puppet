#Make sure tmpfs is setup
#https://insights.ubuntu.com/2016/01/20/data-driven-analysis-tmp-on-tmpfs/

  mount { '/tmp':
     ensure => $use_tmpfs,
     device => 'tmpfs',
     fstype => 'tmpfs',
     options => 'rw,nosuid,nodev',
  }
