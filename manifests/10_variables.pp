#Set vairables to be used by other manifests
  $rootgroup = $facts['os']['family'] ? {
    'RedHat'          => 'wheel',
    'Debian'          => 'adm',
    /(Darwin|FreeBSD)/ => 'wheel',
    default            => 'root',
  }

  $sshgrp = lookup ( { name => 'sshkey::users_group', default_value => 'sshkey_users' } )

  $sshgrpid = lookup ( { name => 'sshkey::users_group_id', default_value => undef} )
 

  $admin_groups = [ $rootgroup, $sshgrp]

  $use_tmpfs = lookup ( { name => 'tmpfs::ensure', default_value => 'mounted'} )
